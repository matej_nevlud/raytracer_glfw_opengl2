#ifndef VECTOR3_H_
#define VECTOR3_H_

#include "structs.h"

#include "mymath.h"
/*! \struct Vector3
\brief Trojrozměrný (3D) vektor.

Implementace tříslokového reálného vektoru podporující základní
matematické operace.

\note
Vektor se povauje za sloupcový, přestoe je v komentářích pro jednoduchost
uváděn jako řádkový.

\code{.cpp}
Vector3 v = Vector3( 2.0f, 4.5f, 7.8f );
v.Normalize();
\endcode

\author Tomá Fabián
\version 0.95
\date 2007-2015
*/
struct /*ALIGN*/ Vector3
{
public:
	union	// anonymní unie
	{
		struct
		{
			float x; /*!< První sloka vektoru. */
			float y; /*!< Druhá sloka vektoru. */
			float z; /*!< Třetí sloka vektoru. */
		};

		float data[3]; /*!< Pole sloek vektoru. */
	};

	//! Výchozí konstruktor.
	/*!
	Inicializuje vechny sloky vektoru na hodnotu nula,
	\f$\mathbf{v}=\mathbf{0}\f$.
	*/
	Vector3() : x( 0 ), y( 0 ), z( 0 ) { }	

	//! Obecný konstruktor.
	/*!
	Inicializuje sloky vektoru podle zadaných hodnot parametrů,
	\f$\mathbf{v}=(x,y,z)\f$.

	\param x první sloka vektoru.
	\param y druhá sloka vektoru.
	\param z třetí sloka vektoru.
	*/
	Vector3( const float x, const float y, const float z ) : x( x ), y( y ), z( z ) { }

	//! Konstruktor z pole.
	/*!
	Inicializuje sloky vektoru podle zadaných hodnot pole,

	\param v ukazatel na první sloka vektoru.	
	*/
	Vector3( const float * v );

	//! L2-norma vektoru.
	/*!
	\return x Hodnotu \f$\mathbf{||v||}=\sqrt{x^2+y^2+z^2}\f$.
	*/
	float L2Norm() const;

	//! Druhá mocnina L2-normy vektoru.
	/*!
	\return Hodnotu \f$\mathbf{||v||^2}=x^2+y^2+z^2\f$.
	*/
	float SqrL2Norm() const;

	//! Normalizace vektoru.
	/*!
	Po provedení operace bude mít vektor jednotkovou délku.
	*/
	void Normalize();

	Vector3 NormalizeReturn();

	static Vector3 NormalizeStatic(Vector3 v);

	//! Vektorový součin.
	/*!
	\param v vektor \f$\mathbf{v}\f$.

	\return Vektor \f$(\mathbf{u}_x \mathbf{v}_z - \mathbf{u}_z \mathbf{v}_y,
	\mathbf{u}_z \mathbf{v}_x - \mathbf{u}_x \mathbf{v}_z,
	\mathbf{u}_x \mathbf{v}_y - \mathbf{u}_y \mathbf{v}_x)\f$.
	*/
	Vector3 CrossProduct( const Vector3 & v ) const;	

	Vector3 Abs() const;

	Vector3 Max( const float a = 0 ) const;

	//! Skalární součin.
	/*!		
	\return Hodnotu \f$\mathbf{u}_x \mathbf{v}_x + \mathbf{u}_y \mathbf{v}_y + \mathbf{u}_z \mathbf{v}_z)\f$.
	*/
	float DotProduct( const Vector3 & v ) const;	

	//! Index největší složky vektoru.
	/*!
	\param absolute_value index bude určen podle absolutní hodnoty sloky

	\return Index největí sloky vektoru.
	*/
	char LargestComponent( const bool absolute_value = false );	

	void Print();

	Vector3 RandomUnitVector();

	Color3f ToColor3f() {
		return {this->x, this->y, this->z};
	}

	static Vector3 RandomUnitVectorDof() {
		auto x = random_double(-1, 1);
		auto y = random_double(-1, 1);
		return Vector3(x, y, 0);
	}

	// --- operátory ------

	friend Vector3 operator-( const Vector3 & v );

	friend Vector3 operator+( const Vector3 & u, const Vector3 & v );
	friend Vector3 operator-( const Vector3 & u, const Vector3 & v );

	friend Vector3 operator*( const Vector3 & v, const float a );	
	friend Vector3 operator*( const float a, const Vector3 & v );
	friend Vector3 operator*( const Vector3 & u, const Vector3 & v );

	friend Vector3 operator/( const Vector3 & v, const float a );

	friend void operator+=( Vector3 & u, const Vector3 & v );
	friend void operator-=( Vector3 & u, const Vector3 & v );
	friend void operator*=( Vector3 & v, const float a );
	friend void operator/=( Vector3 & v, const float a );		
};

#endif
