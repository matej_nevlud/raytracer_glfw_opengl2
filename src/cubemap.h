#pragma once

#include <string>
#include <memory>
#include <array>
#include "structs.h"
#include "vector3.h"

class Texture;

class CubeMap {

public:
	CubeMap();
	CubeMap( const char* filename);
	~CubeMap();

	Color3f get_texel( Vector3 & direction );

private:
	std::array<Texture *, 6> maps;
    
};
