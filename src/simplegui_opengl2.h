#pragma once
#include "shared.h"
#include "simplegui_opengl2.h"


enum Shader {
	SHADER_OFF,
	SHADER_NORMAL,
	SHADER_LAMBERT,
	SHADER_MATERIAL,
	SHADER_PHONG,
	SHADER_DIFFUSE,
	SHADER_WHITTED

};

int runDemo();


class SimpleGuiOpenGL2 {

public:	
	SimpleGuiOpenGL2( const int w, const int h );	
	~SimpleGuiOpenGL2();		
	
	int MainLoop();	

protected:
	int Init();
	int Cleanup();	

	void CreateRenderTarget();
	void CleanupRenderTarget();
	void CleanupDeviceD3D();

	void CreateTexture();
	void UpdateTexture();

	virtual int Ui();
	virtual Color4f get_pixel( const int x, const int y, const float t = 0.0f );

	//void Producer(SimpleGuiOpenGL2* self);
	void Producer();

	int width = 800;
	int height = 600;

	bool vsync_{ true };

private:
    GLFWwindow * window = nullptr;
    //GLuint * image_texture = nullptr;
    GLuint image_texture;


	float * tex_data_ = nullptr; // DXGI_FORMAT_R32G32B32A32_FLOAT
	int * frame_counter = nullptr; // DXGI_FORMAT_R32G32B32A32_FLOAT
	int frame_counterS = 0; // DXGI_FORMAT_R32G32B32A32_FLOAT
		
};
