#include "stdafx.h"
#include "raytracer.h"
#include "objloader.h"
#include "tutorials.h"
#include <algorithm>
#include "mymath.h"
#include "cubemap.h"

Raytracer::Raytracer( const int width, const int height, const float fov_y, const Vector3 view_from, const Vector3 view_at, const char * config ) : SimpleGuiOpenGL2( width, height )
{
	InitDeviceAndScene( config );

	camera_ = Camera( width, height, fov_y, view_from, view_at );

	currentShader = SHADER_WHITTED;

	cubemap = new CubeMap("./data/cubemap/yokohama/");
}

Raytracer::~Raytracer()
{
	ReleaseDeviceAndScene();
}

int Raytracer::InitDeviceAndScene( const char * config )
{
	device_ = rtcNewDevice( config );
	error_handler( nullptr, rtcGetDeviceError( device_ ), "Unable to create a new device.\n" );
	rtcSetDeviceErrorFunction( device_, error_handler, nullptr );

	ssize_t triangle_supported = rtcGetDeviceProperty( device_, RTC_DEVICE_PROPERTY_TRIANGLE_GEOMETRY_SUPPORTED );

	// create a new scene bound to the specified device
	scene_ = rtcNewScene( device_ );

	return 0;
}

int Raytracer::ReleaseDeviceAndScene()
{
	rtcReleaseScene( scene_ );
	rtcReleaseDevice( device_ );

	return 0;
}

void Raytracer::LoadScene( const std::string file_name )
{
	const int no_surfaces = LoadOBJ( file_name.c_str(), surfaces_, materials_ );

	// surfaces loop
	for ( auto surface : surfaces_ )
	{
		RTCGeometry mesh = rtcNewGeometry( device_, RTC_GEOMETRY_TYPE_TRIANGLE );

		Vertex3f * vertices = ( Vertex3f * )rtcSetNewGeometryBuffer(
			mesh, RTC_BUFFER_TYPE_VERTEX, 0, RTC_FORMAT_FLOAT3,
			sizeof( Vertex3f ), 3 * surface->no_triangles() );

		Triangle3ui * triangles = ( Triangle3ui * )rtcSetNewGeometryBuffer(
			mesh, RTC_BUFFER_TYPE_INDEX, 0, RTC_FORMAT_UINT3,
			sizeof( Triangle3ui ), surface->no_triangles() );

		rtcSetGeometryUserData( mesh, ( void* )( surface->get_material() ) );

		rtcSetGeometryVertexAttributeCount( mesh, 2 );

		Normal3f * normals = ( Normal3f * )rtcSetNewGeometryBuffer(
			mesh, RTC_BUFFER_TYPE_VERTEX_ATTRIBUTE, 0, RTC_FORMAT_FLOAT3,
			sizeof( Normal3f ), 3 * surface->no_triangles() );

		Coord2f * tex_coords = ( Coord2f * )rtcSetNewGeometryBuffer(
			mesh, RTC_BUFFER_TYPE_VERTEX_ATTRIBUTE, 1, RTC_FORMAT_FLOAT2,
			sizeof( Coord2f ), 3 * surface->no_triangles() );		

		// triangles loop
		for ( int i = 0, k = 0; i < surface->no_triangles(); ++i )
		{
			Triangle & triangle = surface->get_triangle( i );

			// vertices loop
			for ( int j = 0; j < 3; ++j, ++k )
			{
				const Vertex & vertex = triangle.vertex( j );

				vertices[k].x = vertex.position.x;
				vertices[k].y = vertex.position.y;
				vertices[k].z = vertex.position.z;

				normals[k].x = vertex.normal.x;
				normals[k].y = vertex.normal.y;
				normals[k].z = vertex.normal.z;

				tex_coords[k].u = vertex.texture_coords[0].u;
				tex_coords[k].v = vertex.texture_coords[0].v;
			} // end of vertices loop

			triangles[i].v0 = k - 3;
			triangles[i].v1 = k - 2;
			triangles[i].v2 = k - 1;
		} // end of triangles loop

		rtcCommitGeometry( mesh );
		unsigned int geom_id = rtcAttachGeometry( scene_, mesh );
		rtcReleaseGeometry( mesh );
	} // end of surfaces loop

	rtcCommitScene( scene_ );
}

Color3f Raytracer::PhongIlluminationModel(RTCRayHit ray_hit, Vector3 ray_direction, Vector3 hit_position, RTCGeometry geometry) {
	// get interpolated normal
	Vector3 normal;
	rtcInterpolate0( geometry, ray_hit.hit.primID, ray_hit.hit.u, ray_hit.hit.v,
		RTC_BUFFER_TYPE_VERTEX_ATTRIBUTE, 0, &normal.x, 3 );
	
	// check if normal is pointing outside of mesh, not in
	normal.Normalize();
	if(normal.DotProduct(ray_direction) > 0) normal = -normal;

	Coord2f tex_coord;
	rtcInterpolate0( geometry, ray_hit.hit.primID, ray_hit.hit.u, ray_hit.hit.v,
		RTC_BUFFER_TYPE_VERTEX_ATTRIBUTE, 1, &tex_coord.u, 2 );
	Material* hit_material = ( Material * )( rtcGetGeometryUserData( geometry ) );
	Texture* hit_texture_diffuse = hit_material->get_texture(Material::kDiffuseMapSlot);
	Texture* hit_texture_specular = hit_material->get_texture(Material::kSpecularMapSlot);
	

	normal.Normalize();
	Vector3 normalRGB = (normal + Vector3{1.0f, 1.0f, 1.0f}) / 2.0f; 
	Vector3 light_direction = light_position - hit_position;
	light_direction.Normalize();

	float diffuse = normal.DotProduct(light_direction);
	if(diffuse < 0) diffuse = 0.0f;
	
	Vector3 reflected_light_direction = 2.0 * (normal.DotProduct(light_direction)) * normal - light_direction;
	//float specular = pow(max((ray_direction).DotProduct(reflected_light_direction), 0.0f) , hit_material->shininess);
	float specular = pow(max((ray_direction).DotProduct(-reflected_light_direction), 0.0f) , hit_material->shininess);
	
	

	Color3f diffuseC = (hit_texture_diffuse ? hit_texture_diffuse->get_texel(tex_coord.u, 1 - tex_coord.v) : hit_material->diffuse.ToColor3f()).AsLinear() * diffuse;
	Color3f specularC = (hit_texture_specular ? hit_texture_specular->get_texel(tex_coord.u, 1 - tex_coord.v) : hit_material->specular.ToColor3f()).AsLinear() * specular;
	Color3f ambientC = hit_material->ambient.ToColor3f().AsLinear();

	//float visibility = ShadowRay_IsLightObscured(p_rayHit, p_light) ? 0.f : 1.f;
	float visibility = 1.f;
	Color3f colorPhong = (ambientC + (diffuseC + specularC) * visibility);

	return colorPhong;
}

RTCRayHit Raytracer::TraceRay(RTCRay ray, RTCScene scene_, int reflectCount) {
	// setup a hit
	RTCHit hit;
	hit.geomID = RTC_INVALID_GEOMETRY_ID;
	hit.primID = RTC_INVALID_GEOMETRY_ID;
	hit.Ng_x = 0.0f; // geometry normal
	hit.Ng_y = 0.0f;
	hit.Ng_z = 0.0f;

	// merge ray and hit structures
	RTCRayHit ray_hit;
	ray_hit.ray = ray;
	ray_hit.hit = hit;

	// intersect ray with the scene
	RTCIntersectContext context;
	rtcInitIntersectContext( &context );
	rtcIntersect1( scene_, &context, &ray_hit );

	return ray_hit;
}

RTCRay Raytracer::PrepareRay(Vector3 origin, Vector3 direction) {
	RTCRay ray = RTCRay();

	direction.Normalize();
	ray.dir_x = direction.x;
	ray.dir_y = direction.y;
	ray.dir_z = direction.z;

	origin += direction * 0.0001;

	ray.org_x = origin.x;
	ray.org_y = origin.y;
	ray.org_z = origin.z;

	
	ray.tnear = FLT_MIN;
	ray.tfar = FLT_MAX;

	return ray;
}


Vector3 RefractVector(const Vector3& incident, const Vector3& normal, float n1, float n2) {
   float n = n1 / n2;
   float cosI = -normal.DotProduct(incident);
   float sinT2 = n * n * (1.0 - cosI * cosI);

   if (sinT2 > 1.0) {
      cerr << "Bad refraction vector!" << endl;
      exit(EXIT_FAILURE);
   }

   float cosT = sqrt(1.0 - sinT2);
   return incident * n + normal * (n * cosI - cosT);
}

float GetReflectance(Vector3 &incident, Vector3 &normal, float n1, float n2) {
	float n = n1 / n2;
	float cosI = -normal.DotProduct(incident);
	float sinT2 = n * n * (1.0 - cosI * cosI);

	if (sinT2 > 1.0) {
		// Total Internal Reflection.
		return 1.0;
	}

	float cosT = sqrt(1.0 - sinT2);
	float r0rth = (n1 * cosI - n2 * cosT) / (n1 * cosI + n2 * cosT);
	float rPar = (n2 * cosI - n1 * cosT) / (n2 * cosI + n1 * cosT);
	return (r0rth * r0rth + rPar * rPar) / 2.0;
}

Color3f Raytracer::WhittedRayTracer(RTCRay ray, int reflectCount, float n1, bool fromAir) {

	RTCRayHit ray_hit = TraceRay(ray, scene_);

	// remember ray direction from camera
	Vector3 ray_origin = Vector3{ray_hit.ray.org_x, ray_hit.ray.org_y, ray_hit.ray.org_z};
	Vector3 ray_direction = Vector3{ray_hit.ray.dir_x, ray_hit.ray.dir_y, ray_hit.ray.dir_z};
	Vector3 hit_position = ray_origin + ray_direction * ray_hit.ray.tfar;
	Vector3 light_direction = (light_position - hit_position).NormalizeReturn();

	// TODO: Return background color from CUBE MAP
	if ( ray_hit.hit.geomID == RTC_INVALID_GEOMETRY_ID )
		return cubemap->get_texel(ray_direction);

	// Stop on reflection count limit
	if(reflectCount >= 5)
		return Color3f{ 1.0, 1.0, 1.0};


	// we hit something
	RTCGeometry geometry = rtcGetGeometry( scene_, ray_hit.hit.geomID );	
	Material* hit_material = ( Material * )( rtcGetGeometryUserData( geometry ) );
	//printf("Shader %d\n", hit_material->shader);


	Vector3 normal;
	rtcInterpolate0( geometry, ray_hit.hit.primID, ray_hit.hit.u, ray_hit.hit.v, RTC_BUFFER_TYPE_VERTEX_ATTRIBUTE, 0, &normal.x, 3 );
	// check if normal is pointing outside of mesh, not in
	if(normal.DotProduct(ray_direction) > 0) normal = -normal;	

	// Global scene normal shader
	if (currentShader == SHADER_NORMAL) {
		
		Vector3 normalRGB = {normal.x + 1.0f, normal.y + 1.0f, normal.z + 1.0f };
		normalRGB *= 0.5f;

		return Color3f{ normalRGB.x, normalRGB.y, normalRGB.z};
	}

	// Global scene lambertian shader
	if (currentShader == SHADER_LAMBERT) {

		float lambertCos = light_direction.DotProduct(normal);
		Color3f fr = hit_material->diffuse.ToColor3f().AsLinear() * (float)lambertCos;

		//return fr;
		return Color3f{ lambertCos, lambertCos, lambertCos};
	}

	// Hit diffuse material
	if(hit_material->shader == 4) {
		float R = 1.f;
		float n2 = fromAir ? hit_material->ior : 1.f;
		float iorRatio = n1 / n2;
		float F0 = powf((n1 - n2) / (n1 + n2), 2);
		R = F0 + (1 - F0) * powf((1 - cosf( -normal.DotProduct(ray_direction))), 5);
		R = GetReflectance(ray_direction,  normal, n1, n2);

		// BEGIN REFLECTION
		// 𝑟=𝑑−2(𝑑⋅𝑛)𝑛
		Vector3 vectorReflected = ray_direction - 2 * (ray_direction.DotProduct(normal)) * normal;
		RTCRay rayReflected = PrepareRay(hit_position, vectorReflected);
		Color3f colorReflected = WhittedRayTracer(rayReflected, reflectCount + 1, n1, fromAir) * R;
		// END REFLECTION


		// BEGIN REFRACTION
		Color3f colorRefracted = {0.0f, 0.0f, 0.0f};
		float refractionRootVal = 1 - (iorRatio * iorRatio) * (1 - normal.DotProduct(ray_direction) * normal.DotProduct(ray_direction));
		if(refractionRootVal >= 0) {
			Vector3 vectorRefracted = (iorRatio * ray_direction - (iorRatio * normal.DotProduct(ray_direction) + sqrtf(refractionRootVal)) * normal).NormalizeReturn();
			vectorRefracted = RefractVector(ray_direction, normal, n1, n2);
			RTCRay rayRefracted = PrepareRay(hit_position, vectorRefracted);
			colorRefracted = WhittedRayTracer(rayRefracted, reflectCount + 1, n2, !fromAir) * (1.0f - R);
		}
		
		// END REFRACTION
		float l = fromAir ? 0.f : ray_hit.ray.tfar;
		Color3f T_bl = hit_material->tf.ToColor3f() * -l;
		T_bl = {expf(T_bl.r), expf(T_bl.g), expf(T_bl.b)};
		
		//return colorReflected * T_bl;
		return ((colorReflected + colorRefracted) * T_bl);
	}

	if(hit_material->shader == 3) {
		//Compute phong color model
		
		Color3f colorPhong = PhongIlluminationModel(ray_hit, ray_direction, hit_position, geometry);

		float R = 1.f;
		float n2 = fromAir ? hit_material->ior : 1.f;
		float iorRatio = n1 / n2;
		float F0 = powf((n1 - n2) / (n1 + n2), 2);
		R = F0 + (1 - F0) * powf((1 - cosf( -normal.DotProduct(ray_direction))), 5);
		R = GetReflectance(ray_direction,  normal, n1, n2);

		// BEGIN REFLECTION
		// 𝑟=𝑑−2(𝑑⋅𝑛)𝑛
		Vector3 vectorReflected = ray_direction - 2 * (ray_direction.DotProduct(normal)) * normal;
		RTCRay rayReflected = PrepareRay(hit_position, vectorReflected);
		Color3f colorReflected = WhittedRayTracer(rayReflected, reflectCount + 1) * R;
		// END REFLECTION

		//Compute shadows (is any object between hit position and light ?) 
		// (normal * 0.1) -> we should not hit our own surface that we hit earlier 
		RTCRay light_ray = PrepareRay(hit_position, light_direction); 
		RTCRayHit light_hit = TraceRay(light_ray, scene_); 
		if ( light_hit.hit.geomID != RTC_INVALID_GEOMETRY_ID ) { 
			float shadowIntensity = 0.3f; 
			colorPhong = colorPhong * shadowIntensity; 
		} 

		return (colorPhong + colorReflected);
	}


	return Color3f{ 0, 0, 0};
}

Color4f Raytracer::Trace(RTCRay ray) {

	// setup a hit
	RTCHit hit;
	hit.geomID = RTC_INVALID_GEOMETRY_ID;
	hit.primID = RTC_INVALID_GEOMETRY_ID;
	hit.Ng_x = 0.0f; // geometry normal
	hit.Ng_y = 0.0f;
	hit.Ng_z = 0.0f;

	// merge ray and hit structures
	RTCRayHit ray_hit;
	ray_hit.ray = ray;
	ray_hit.hit = hit;

	// intersect ray with the scene
	RTCIntersectContext context;
	rtcInitIntersectContext( &context );
	rtcIntersect1( scene_, &context, &ray_hit );
	if ( ray_hit.hit.geomID != RTC_INVALID_GEOMETRY_ID )
	{
		// we hit something
		RTCGeometry geometry = rtcGetGeometry( scene_, ray_hit.hit.geomID );
		
		// remember ray direction from camera
		Vector3 ray_origin = Vector3{ray_hit.ray.org_x, ray_hit.ray.org_y, ray_hit.ray.org_z};
		Vector3 ray_direction = Vector3{ray_hit.ray.dir_x, ray_hit.ray.dir_y, ray_hit.ray.dir_z};
		Vector3 hit_position = ray_origin + ray_direction * ray_hit.ray.tfar; 

		// get interpolated normal
		Vector3 normal;
		rtcInterpolate0( geometry, ray_hit.hit.primID, ray_hit.hit.u, ray_hit.hit.v,
			RTC_BUFFER_TYPE_VERTEX_ATTRIBUTE, 0, &normal.x, 3 );
		
		// check if normal is pointing outside of mesh, not in
		normal.Normalize();
		if(normal.DotProduct(ray_direction) > 0) normal = -normal;

		// and texture coordinates
		Coord2f tex_coord;
		rtcInterpolate0( geometry, ray_hit.hit.primID, ray_hit.hit.u, ray_hit.hit.v,
			RTC_BUFFER_TYPE_VERTEX_ATTRIBUTE, 1, &tex_coord.u, 2 );

		Surface* hit_surface = surfaces_[ray_hit.hit.geomID];
		//Material* hit_material = hit_surface->get_material();
		Material * hit_material = ( Material * )( rtcGetGeometryUserData( geometry ) );
		Texture* hit_texture = hit_material->get_texture(Material::kDiffuseMapSlot);
		Triangle hit_triangle = hit_surface->get_triangle(ray_hit.hit.primID);
		

		
		

		if (currentShader == SHADER_NORMAL) {
		
			Vector3 normalRGB = {normal.x + 1.0f, normal.y + 1.0f, normal.z + 1.0f };
			normalRGB *= 0.5f;

			return Color4f{ normalRGB.x, normalRGB.y, normalRGB.z, 1.0f };
		}

		if (currentShader == SHADER_MATERIAL) {
			try {
								// Check if material has any texture
				if(hit_texture != nullptr){
					// Embree texture coordinates has swapped u & v
					Color3f hit_texel = hit_texture->get_texel(tex_coord.u, 1 - tex_coord.v);
					return Color4f{hit_texel.r, hit_texel.g, hit_texel.b, 1.0f};
				}
				return Color4f{ hit_material->diffuse.x, hit_material->diffuse.y, hit_material->diffuse.z, 1.0f };

			} catch(...) {
				printf("Out of bounds");
			}
		
		}

		if (currentShader == SHADER_DIFFUSE) {
			normal.Normalize();
			Vector3 normalRGB = (normal + Vector3{1.0f, 1.0f, 1.0f}) / 2.0f; 
			Vector3 light_direction = light_position - hit_position;
			light_direction.Normalize();

			float diffuse = normal.DotProduct(light_direction);
			if(diffuse < 0) diffuse = 0.0f;
			
			Vector3 reflected_light_direction = 2.0 * (normal.DotProduct(light_direction)) * normal - light_direction;
			float specular = (float) 2 * pow((ray_direction).DotProduct(reflected_light_direction) , 80);
			
			
			// Check if material has any texture
			if(hit_texture != nullptr){
				// Embree texture coordinates has swapped u & v
				Color3f hit_texel = hit_texture->get_texel(tex_coord.u, 1 - tex_coord.v);
				return Color4f{ hit_texel.r * diffuse + hit_texel.r * specular, 
								hit_texel.g * diffuse + hit_texel.g * specular, 
								hit_texel.b * diffuse + hit_texel.b * specular, 1.0f};
			}

			float colorR = hit_material->diffuse.x * diffuse + hit_material->specular.x * specular;
			float colorG = hit_material->diffuse.y * diffuse + hit_material->specular.y * specular;
			float colorB = hit_material->diffuse.z * diffuse + hit_material->specular.z * specular;
			return Color4f{ colorR, colorG, colorB, 1.0f };
		}

		if (currentShader == SHADER_WHITTED) {
			normal.Normalize();
			Vector3 normalRGB = (normal + Vector3{1.0f, 1.0f, 1.0f}) / 2.0f; 
			Vector3 light_direction = light_position - hit_position;
			light_direction.Normalize();

			float diffuse = normal.DotProduct(light_direction);
			if(diffuse < 0) diffuse = 0.0f;
			
			Vector3 reflected_light_direction = 2.0 * (normal.DotProduct(light_direction)) * normal - light_direction;
			//float specular = pow(max((ray_direction).DotProduct(reflected_light_direction), 0.0f) , hit_material->shininess);
			float specular = pow((ray_direction).DotProduct(reflected_light_direction) , hit_material->shininess);
			
			
			// Check if material has any texture
			if(hit_texture != nullptr){
				// Embree texture coordinates has swapped u & v
				Color3f hit_texel = hit_texture->get_texel(tex_coord.u, 1 - tex_coord.v);
				return Color4f{ hit_texel.r * diffuse + hit_texel.r * specular, 
								hit_texel.g * diffuse + hit_texel.g * specular, 
								hit_texel.b * diffuse + hit_texel.b * specular, 1.0f};
			}

			float colorR = hit_material->diffuse.x * diffuse + hit_material->specular.x * specular;
			float colorG = hit_material->diffuse.y * diffuse + hit_material->specular.y * specular;
			float colorB = hit_material->diffuse.z * diffuse + hit_material->specular.z * specular;
			return Color4f{ colorR, colorG, colorB, 1.0f };
		}
		

		//printf( "normal = (%0.3f, %0.3f, %0.3f)\n", normal.x, normal.y, normal.z );
		//printf( "tex_coord = (%0.3f, %0.3f)\n", tex_coord.u, tex_coord.v );
		//return Color4f{(ray_hit.hit.Ng_x + 1.0f) / 2.0f, (ray_hit.hit.Ng_y + 1.0f) / 2.0f, (ray_hit.hit.Ng_z + 1.0f) / 2.0f, 1.0f};
		return Color4f{ normal.x, normal.y, normal.z, 1.0f };
		
	} else {
		return Color4f{ 1.0f, 1.0f, 1.0f, 1.0f };
		return Color4f{ 0, 0, 0, 1.0f };
	}
}


std::default_random_engine generator;
std::uniform_real_distribution<double> pixelSample(-0.5,0.5);

Color4f Raytracer::get_pixel( const int x, const int y, const float t )
{	
	RTCRay ray;// = camera_.GenerateRay(x, y);
	ray.time = t;
	//return  WhittedRayTracer(ray);

	Color3f fullPixel = Color3f{0.f,0.f,0.f};
	std::uniform_real_distribution<float> distrib{ -0.5f, 0.5f };

	//DEPTH OF FIELD
	if(dof) {
		std::vector<RTCRay> rays = camera_.GenerateRayDof(x, y, dofFocalLen, dofApertureSize, dofApertureSample);

		for( int i = 1; i < dofApertureSample; i++) {
			fullPixel += WhittedRayTracer(rays[i]);
		}

		ray = camera_.GenerateRay(x, y);
		fullPixel += WhittedRayTracer(ray);	
		fullPixel = fullPixel / dofApertureSample;
	} else {
		//SUPERSAMPLING

		for( int i = 1; i < perPixelSampling; i++) {
			ray = camera_.GenerateRay(x + pixelSample(generator), y + pixelSample(generator));
			fullPixel += WhittedRayTracer(ray);
		}

		ray = camera_.GenerateRay(x, y);
		fullPixel += WhittedRayTracer(ray);	
		fullPixel = fullPixel / perPixelSampling;
	}
	

	return fullPixel;
}

int Raytracer::Ui()
{
	static float f = 0.0f;
	static int counter = 0;

	
	ImGui::Text( "Surfaces = %d", surfaces_.size() );
	ImGui::Text( "Materials = %d", materials_.size() );
	ImGui::Separator();
	ImGui::Checkbox( "Vsync", &vsync_ );
	
	//ImGui::Checkbox( "Demo Window", &show_demo_window ); // Edit bools storing our window open/close state
	//ImGui::Checkbox( "Another Window", &show_another_window );

	ImGui::SliderFloat( "float", &f, 0.0f, 1.0f ); // Edit 1 float using a slider from 0.0f to 1.0f    

	ImGui::Separator();
	ImGui::Text( "Camera Position" );
	
	ImGui::SliderFloat( "X Pos", &camera_.view_from_.x, -300.0f, 300.0f ); // Edit 1 float using a slider from -300.0f to 300.0f    
	ImGui::SliderFloat( "Y Pos", &camera_.view_from_.y, -300.0f, 300.0f ); // Edit 1 float using a slider from -300.0f to 300.0f    
	ImGui::SliderFloat( "Z Pos", &camera_.view_from_.z, -300.0f, 300.0f ); // Edit 1 float using a slider from -300.0f to 300.0f    

	ImGui::Separator();

	ImGui::SliderInt( "Pixel Sampling", &perPixelSampling, 1, 32 );
	ImGui::SliderInt( "Aperture Samples", &dofApertureSample, 1, 32 );
	ImGui::SliderFloat( "Aperture Size", &dofApertureSize, 0.0f, 10.0f );
	ImGui::SliderFloat( "Focal Length", &dofFocalLen, 0.0f, 500.0f );
	ImGui::Checkbox("Depth Of Field", &dof);
	
	//ImGui::ColorEdit3( "clear color", ( float* )&clear_color ); // Edit 3 floats representing a color

	// Buttons return true when clicked (most widgets return true when edited/activated)
	ImGui::Separator();
	if ( ImGui::Button( "Normal Shader" ) ) currentShader = SHADER_NORMAL;
	if ( ImGui::Button( "Lambert Shader" ) ) currentShader = SHADER_LAMBERT;
	ImGui::SameLine();
	if ( ImGui::Button( "Material Shader" ) ) currentShader = SHADER_MATERIAL;

	if ( ImGui::Button( "Phong Shader" ) ) currentShader = SHADER_PHONG;
	ImGui::SameLine();
	if ( ImGui::Button( "Diffuse Shader" ) ) currentShader = SHADER_DIFFUSE;
	if ( ImGui::Button( "Whitted Shader" ) ) currentShader = SHADER_WHITTED;

	

	// 3. Show another simple window.
	/*if ( show_another_window )
	{
	ImGui::Begin( "Another Window", &show_another_window ); // Pass a pointer to our bool variable (the window will have a closing button that will clear the bool when clicked)
	ImGui::Text( "Hello from another window!" );
	if ( ImGui::Button( "Close Me" ) )
	show_another_window = false;
	ImGui::End();
	}*/

	return 0;
}
