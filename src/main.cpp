// dear imgui: standalone example application for GLFW + OpenGL2, using legacy fixed pipeline
// If you are new to dear imgui, see examples/README.txt and documentation at the top of imgui.cpp.
// (GLFW is a cross-platform general purpose library for handling windows, inputs, OpenGL/Vulkan/Metal graphics context creation, etc.)

// **DO NOT USE THIS CODE IF YOUR CODE/ENGINE IS USING MODERN OPENGL (SHADERS, VBO, VAO, etc.)**
// **Prefer using the code in the example_glfw_opengl2/ folder**
// See imgui_impl_glfw.cpp for details.

#include "shared.h"
#include "simplegui_opengl2.h"
#include "raytracer.h"
#include "mymath.h"



int main(int, char**) {

	//Vector3 viewFrom = Vector3{-76, -42, 32};
	Vector3 viewFrom = Vector3( -175, -140, 130 );


    Raytracer raytracer( 800, 600, 
						deg2rad( 45 ),
						viewFrom, 
						Vector3( 0, 0, 35 ), 
						nullptr );

	raytracer.LoadScene( "./data/6887_allied_avenger.obj" );
	//raytracer.LoadScene( "./data/test_scene_whitted/test_scene_whitted.obj" );
	//raytracer.LoadScene( "./data/BC304Render.obj" );
	//raytracer.LoadScene( "./data/BC304Render.obj" );
	raytracer.MainLoop();

    return 0;
}

