#pragma once

#include "imgui.h"
#include "imgui_impl_glfw.h"
#include "imgui_impl_opengl2.h"
#include <stdio.h>
#ifdef __APPLE__
#define GL_SILENCE_DEPRECATION
#endif
#include <GLFW/glfw3.h>
// Intel Embree 3.6.1 (high performance ray tracing kernels)
#include <embree3/rtcore.h>

#include "structs.h"
#include "color.h"