#pragma once

//#include "targetver.h"

#define _CRT_SECURE_NO_WARNINGS

// std libs
#include <stdio.h>
#include <cstdlib>
#include <string>
#include <chrono>
#include <mutex>
#include <thread>
#include <atomic>
//#include <tchar.h>
#include <vector>
#include <map>
#include <random>
#include <functional>
#include <algorithm>

#include <xmmintrin.h>
#include <pmmintrin.h>

// Intel Embree 3.6.1 (high performance ray tracing kernels)
#include <embree3/rtcore.h>

#include "imgui.h"
#include "imgui_impl_glfw.h"
#include "imgui_impl_opengl2.h"
#include <stdio.h>
#ifdef __APPLE__
#define GL_SILENCE_DEPRECATION
#endif
#include <GLFW/glfw3.h>
