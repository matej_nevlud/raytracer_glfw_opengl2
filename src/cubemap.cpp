#include "stdafx.h"
#include "cubemap.h"
#include "texture.h"



CubeMap::CubeMap( ) {
    
    maps[0] = new Texture("opa");
    maps[1] = new Texture("opa");
    maps[2] = new Texture("opa");
    maps[3] = new Texture("opa");
    maps[4] = new Texture("opa");
    maps[5] = new Texture("opa");
    
}
CubeMap::CubeMap( const char* filename) {
    
    maps[0] = new Texture((std::string(filename) + std::string("posx.jpg")).c_str());
    maps[1] = new Texture((std::string(filename) + std::string("negx.jpg")).c_str());
    maps[2] = new Texture((std::string(filename) + std::string("posy.jpg")).c_str());
    maps[3] = new Texture((std::string(filename) + std::string("negy.jpg")).c_str());
    maps[4] = new Texture((std::string(filename) + std::string("posz.jpg")).c_str());
    maps[5] = new Texture((std::string(filename) + std::string("negz.jpg")).c_str());
    
}


Color3f CubeMap::get_texel( Vector3 & direction ) {


    
    int axisIdx = direction.LargestComponent(true);
    
	float tmp, u, v = 0;

    switch (axisIdx) {

    case 0:
        if (direction.x > 0){
            tmp = 1.f / abs(direction.x);
            u = 1 - (direction.y * tmp + 1) * 0.5f;
	        v = 1 - (direction.z * tmp + 1) * 0.5f;
            return maps[0]->get_texel(u, v);
        } else {
            tmp = 1.f / abs(direction.x);
            u = (direction.y * tmp + 1) * 0.5f;
	        v = 1 - (direction.z * tmp + 1) * 0.5f;
            return maps[1]->get_texel(u, v);
        }
        break;

    case 1:
        if (direction.y > 0){
            tmp = 1.f / abs(direction.y);
            u = (direction.x * tmp + 1) * 0.5f;
	        v = 1 - (direction.z * tmp + 1) * 0.5f;
            return maps[2]->get_texel(u, v);
        } else {
            tmp = 1.f / abs(direction.y);
            u = 1 - (direction.x * tmp + 1) * 0.5f;
	        v = 1 - (direction.z * tmp + 1) * 0.5f;
            return maps[3]->get_texel(u, v);
        }
        break;

    case 2:
        if (direction.z > 0){
            tmp = 1.f / abs(direction.z);
            u = 1 - (direction.y * tmp + 1) * 0.5f;
	        v = (direction.x * tmp + 1) * 0.5f;
            return maps[4]->get_texel(u, v);
        } else {
            tmp = 1.f / abs(direction.z);
            u = 1 - (direction.y * tmp + 1) * 0.5f;
	        v = 1 - (direction.x * tmp + 1) * 0.5f;
            return maps[5]->get_texel(u, v);
        }
        break;

    default:
        break;
    }

	

	
	
    return Color3f{ 1, 1, 1};
} 