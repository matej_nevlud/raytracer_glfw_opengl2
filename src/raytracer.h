#pragma once
#include "simplegui_opengl2.h"
#include "surface.h"
#include "camera.h"
#include "cubemap.h"
#include "color.h"
#include <iostream>
#include <vector>
/*! \class Raytracer
\brief General ray tracer class.

\author Tom� Fabi�n
\version 0.1
\date 2018
*/

using namespace std;



class Raytracer : public SimpleGuiOpenGL2
{
public:
	Raytracer( const int width, const int height, 
		const float fov_y, const Vector3 view_from, const Vector3 view_at,
		const char * config = "threads=0,verbose=3" );
	~Raytracer();

	int InitDeviceAndScene( const char * config );

	int ReleaseDeviceAndScene();

	void LoadScene( const string file_name );
	
	RTCRayHit TraceRay(RTCRay ray, RTCScene scene_, int reflectCount = 0);

	RTCRay PrepareRay(Vector3 origin, Vector3 direction);

	Color3f PhongIlluminationModel(RTCRayHit ray_hit, Vector3 ray_direction, Vector3 hit_position, RTCGeometry geometry);

	Color3f WhittedRayTracer(RTCRay ray, int reflectCount = 0, float n1 = 1.0f, bool fromAir = true);

	Color4f Trace(RTCRay ray);

	Color4f get_pixel( const int x, const int y, const float t = 0.0f ) override;

	int Ui();

private:
	vector<Surface* >  surfaces_;
	vector<Material* >  materials_;

	RTCDevice device_;
	RTCScene scene_;
	Camera camera_;

	//Vector3 light_position = Vector3( -175, 140, 150 );
	Vector3 light_position = Vector3( -175.f, -150.f, 130.f );

	float xPosition = 0.0f;

	const int maxReflectCount = 10;

	Shader currentShader;

	CubeMap * cubemap;


	// UI variables
	bool dof = true;
	float dofFocalLen = 200;
	int dofApertureSample = 3;
	float dofApertureSize = 1.0f;
	int perPixelSampling = 1;
};
