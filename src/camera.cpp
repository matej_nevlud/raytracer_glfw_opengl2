#include "stdafx.h"
#include "camera.h"

Camera::Camera( const int width, const int height, const float fov_y, const Vector3 view_from, const Vector3 view_at )
{
	width_ = width;
	height_ = height;
	fov_y_ = fov_y;

	view_from_ = view_from;
	view_at_ = view_at;

	// TODO compute focal lenght based on the vertical field of view and the camera resolution
	// f_y_ = ...
	this->f_y_ = height / (2 * tan(fov_y * 0.5f));

	// TODO build M_c_w_ matrix	
	GenerateTransformMatrixBetweenSpaces();
}

void Camera::GenerateTransformMatrixBetweenSpaces() {
	Vector3 z_c = view_from_ - view_at_;
	z_c.Normalize();

	Vector3 x_c = up_.CrossProduct(z_c);
	x_c.Normalize();

	Vector3 y_c = z_c.CrossProduct(x_c);

	Vector3 z_c_noUnit = view_from_ - view_at_;
	Vector3 x_c_noUnit = up_.CrossProduct(z_c_noUnit);
	Vector3 y_c_noUnit = z_c_noUnit.CrossProduct(x_c_noUnit);

	y_c_noUnit.Normalize();


	M_c_w_ = Matrix3x3( x_c, y_c, z_c);
}

RTCRay Camera::GenerateRay( float x_i, float y_i ) {
	
	RTCRay ray = RTCRay();

	// TODO fill in ray structure and compute ray direction
	// ray.org_x = ...	
	GenerateTransformMatrixBetweenSpaces();

	Vector3 d_c = Vector3(x_i - width_ * 0.5f, height_ / 2 - y_i, - f_y_);
	d_c.Normalize();
	Vector3 d_w = M_c_w_ * d_c;
	
	ray.org_x = this->view_from_.x;
	ray.org_y = this->view_from_.y;
	ray.org_z = this->view_from_.z;

	d_w.Normalize();
	direction = d_w;
	ray.dir_x = d_w.x;
	ray.dir_y = d_w.y;
	ray.dir_z = d_w.z;

	
	ray.tnear = FLT_MIN;
	ray.tfar = FLT_MAX;

	return ray;
}

std::vector<RTCRay> Camera::GenerateRayDof( float x_i, float y_i, float dofFocalLen, float dofApertureSize, int dofSamples) {
	std::vector<RTCRay> rays;

	RTCRay primaryRay = GenerateRay(x_i, y_i);
	
	Vector3 pointP = view_from_ + direction * dofFocalLen;


	for (int i = 0; i < dofSamples; i++)
	{
		RTCRay ray = RTCRay();
		
		Vector3 randomAperturePoint = view_from_ + ( M_c_w_ * Vector3::RandomUnitVectorDof()) * dofApertureSize;

		ray.org_x = randomAperturePoint.x;
		ray.org_y = randomAperturePoint.y;
		ray.org_z = randomAperturePoint.z;

		Vector3 directionToPointP = pointP - randomAperturePoint;
		directionToPointP.Normalize();

		ray.dir_x = directionToPointP.x;
		ray.dir_y = directionToPointP.y;
		ray.dir_z = directionToPointP.z;

		ray.tnear = FLT_MIN;
		ray.tfar = FLT_MAX;

		rays.push_back(ray);
	}
	
	return rays;
}
